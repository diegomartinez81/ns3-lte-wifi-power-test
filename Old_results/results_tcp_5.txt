Waf: Entering directory `/home/diego/ns3-versions-5/ns-3-dev-lbt-diego/build'
Waf: Leaving directory `/home/diego/ns3-versions-5/ns-3-dev-lbt-diego/build'
Build commands will be stored in build/compile_commands.json
'build' finished successfully (9.609s)
Running simulation for 2 sec of data transfer; 6 sec overall
Operator A: LTE; number of cells 1; number of UEs 1
Operator B: LTE; number of cells 1; number of UEs 1
LTE duty cycle: requested 1, actual 1, ABS pattern 0000000000000000000000000000000000000000
Total txop duration: 0 seconds.
Total phy arrivals duration: 0 seconds.
--------monitorA----------
Flow 1 (1.0.0.2:49153 -> 7.0.0.2:50000) proto TCP
  Tx Packets: 985
  Tx Bytes:   563224
  TxOffered:  2.2529 Mbps
  Rx Bytes:   563224
  Throughput: 11.7289 Mbps
  Mean delay:  6.451 ms
  Mean jitter:  0.0569513 ms
  Rx Packets: 985
Flow 2 (7.0.0.2:50000 -> 1.0.0.2:49153) proto TCP
  Tx Packets: 494
  Tx Bytes:   25692
  TxOffered:  0.102768 Mbps
  Rx Bytes:   25692
  Throughput: 0.543847 Mbps
  Mean delay:  11.3873 ms
  Mean jitter:  0.601243 ms
  Rx Packets: 494
--------monitorB----------
Flow 1 (2.0.0.2:49153 -> 7.0.0.3:50000) proto TCP
  Tx Packets: 985
  Tx Bytes:   563224
  TxOffered:  2.2529 Mbps
  Rx Bytes:   563224
  Throughput: 11.704 Mbps
  Mean delay:  6.45184 ms
  Mean jitter:  0.0577837 ms
  Rx Packets: 985
Flow 2 (7.0.0.3:50000 -> 2.0.0.2:49153) proto TCP
  Tx Packets: 494
  Tx Bytes:   25692
  TxOffered:  0.102768 Mbps
  Rx Bytes:   25692
  Throughput: 0.543847 Mbps
  Mean delay:  11.3873 ms
  Mean jitter:  0.601243 ms
  Rx Packets: 494
Flow 3 (2.0.0.2:49154 -> 7.0.0.3:50000) proto TCP
  Tx Packets: 985
  Tx Bytes:   563224
  TxOffered:  2.2529 Mbps
  Rx Bytes:   563224
  Throughput: 11.7164 Mbps
  Mean delay:  6.45142 ms
  Mean jitter:  0.0573694 ms
  Rx Packets: 985
Flow 4 (7.0.0.3:50000 -> 2.0.0.2:49154) proto TCP
  Tx Packets: 494
  Tx Bytes:   25692
  TxOffered:  0.102768 Mbps
  Rx Bytes:   25692
  Throughput: 0.543847 Mbps
  Mean delay:  11.3873 ms
  Mean jitter:  0.601243 ms
  Rx Packets: 494
